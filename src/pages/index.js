import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import { graphql } from "gatsby"
import renderMarkdown from "../components/markdown"

const IndexPage = ({ data }) => (
  <Layout>
    <SEO title="Intro to DevOps" />
    { renderMarkdown(data) }
  </Layout>
)

export const indexQuery = graphql`
  {
    markdownRemark(frontmatter: { path: { eq: "/intro-to-devops" } }) {
      html
      tableOfContents
      frontmatter {
        date(formatString: "YYYY MMMM DD")
        path
        title
      }
    }
  }
`

export default IndexPage
