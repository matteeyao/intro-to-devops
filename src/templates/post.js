import React from "react"
import Layout from "../components/layout"
import { graphql } from "gatsby"
import renderMarkdown from "../components/markdown"

export default function Template({ data }) {
  return (
    <Layout>
      <div className="blog-post">
        { renderMarkdown(data) }
      </div>
    </Layout>
  )
}

export const pageQuery = graphql`
  query($path: String!) {
    markdownRemark(frontmatter: { path: { eq: $path } }) {
      html
      tableOfContents
      frontmatter {
        date(formatString: "YYYY MMMM DD")
        path
        title
      }
    }
  }
`
